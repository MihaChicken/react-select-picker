import React, { Component } from 'react';
import './style/App.css';

import SelectPicker from './components/SelectPicker'

const carMakes = [
  'VW', 'Volvo', 'Ford', 'Audi', 'KIA', 'Jeep',
  'BMW', 'Mercedes', 'Chevrolet', 'Honda', 'Hyundai',
  'Nissan', 'Mazda', 'Toyota', 'Opel', 'Range Rover'
]

// Alias should be stored separately from the car makes due to tasks requirement
// Maybe I got the task wrong, but I would prefer to put the alias inside makes collection
const alias = {
  'VOLKSWAGEN': 'VW',
  'RR': 'Range Rover',
  'RangeRover': 'Range Rover'
}

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      make: ''
    }
  }

  render() {
    const {make} = this.state
    return (
      <div className="app">
        <div>
          <SelectPicker
            placeholder="Choose your car"
            items={carMakes}
            alias={alias}
            value={make}
            onChange={(make) => this.setState({make})}
          />
          <br/>
          Result: {make}
        </div>
      </div>
    );
  }
}

export default App;
