import React from 'react';
import ReactDOM from 'react-dom';
import SelectPicker from '../components/SelectPicker';
import AutoScrolledList from '../components/AutoScrolledList';
import renderer from 'react-test-renderer';
import {mount} from 'enzyme';

it('should render correctly', () => {
  const tree = renderer.create(
    <SelectPicker value="" items={['one', 'two']} alias={{}} placeholder="test"/>
  ).toJSON();

  expect(tree).toMatchSnapshot();
});

it('should render correctly with empty items', () => {
  const tree = renderer.create(
    <SelectPicker value="" items={[]} alias={{}} placeholder=""/>
  ).toJSON();

  expect(tree).toMatchSnapshot();
});

it('should render correct aliases', () => {
  const tree = renderer.create(
    <SelectPicker
      value="v"
      items={['v1', 'v2', 'v3', 'alias1','alias2', 'wrongAlias']}
      alias={{'v4': 'alias1', 'v5': 'alias2'}}
      placeholder="test"/>
  ).toJSON();

  expect(tree).toMatchSnapshot();
});

it('should show menu on focus', () => {
  const wrapper = mount(
    <SelectPicker value='' items={['v1', 'v2', 'v3']} alias={{}} placeholder="test"/>
  );
  expect(wrapper.state('isListShown')).toEqual(false)

  wrapper.find('input').simulate('focus')
  expect(wrapper.state('isListShown')).toEqual(true)
});

it('should hide menu on blur with delay', (done) => {
  const wrapper = mount(
    <SelectPicker value='' items={['v1', 'v2', 'v3']} alias={{}} placeholder="test"/>
  );

  wrapper.setState({isListShown: true})
  wrapper.find('input').simulate('blur')
  expect(wrapper.state('isListShown')).toEqual(true)
  setTimeout(() => {
    expect(wrapper.state('isListShown')).toEqual(false)
    done()
  }, 151)
});

it('should open menu onChange', () => {
  const onChange = jest.fn()
  const wrapper = mount(
    <SelectPicker value='' items={['v1', 'v2', 'v3']} alias={{}} placeholder="test" onChange={onChange}/>
  );

  wrapper.find('input').simulate('change', { target: { value: 'Hello' } })

  expect(onChange.mock.calls.length).toEqual(1)
  expect(wrapper.state('isListShown')).toEqual(true)
});

it('should close menu onPick', () => {
  const onChange = jest.fn()
  const wrapper = mount(
    <SelectPicker value='' items={['v1', 'v2', 'v3']} alias={{}} placeholder="test" onChange={onChange}/>
  );

  wrapper.find(AutoScrolledList).prop('onSelect')('v1')

  expect(onChange.mock.calls.length).toEqual(1)
  expect(wrapper.state('isListShown')).toEqual(false)
});

it('should move active class', () => {
  const onChange = jest.fn()
  const wrapper = mount(
    <SelectPicker value='' items={['v1', 'v2', 'v3']} alias={{}} placeholder="test" onChange={onChange}/>
  )
  const input = wrapper.find('input')
  input.simulate('keydown', {keyCode: 40})

  expect(wrapper.state('activeIndex')).toEqual(1)

  input.simulate('keydown', {keyCode: 38})

  expect(wrapper.state('activeIndex')).toEqual(0)

  input.simulate('keydown', {keyCode: 38})

  expect(wrapper.state('activeIndex')).toEqual(2)

  input.simulate('keydown', {keyCode: 40})

  expect(wrapper.state('activeIndex')).toEqual(0)
})

it('should select value on enter', () => {
  const onSelect = jest.fn()
  const wrapper = mount(
    <SelectPicker value='' items={['v1', 'v2', 'v3']} alias={{}} placeholder="test" onChange={onSelect}/>
  )
  const input = wrapper.find('input')

  input.simulate('keydown', {keyCode: 40})
  input.simulate('keydown', {keyCode: 13})

  expect(onSelect.mock.calls[0][0]).toEqual("v2")

})