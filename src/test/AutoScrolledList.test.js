import React from 'react';
import renderer from 'react-test-renderer';
import {mount} from 'enzyme';
import AutoScrolledList from '../components/AutoScrolledList'

it('should render correctly', () => {
  const list = renderer.create(
    <AutoScrolledList items={["one", "two"]} className="test" itemClassName="test-item"/>
  )
  expect(list.toJSON()).toMatchSnapshot()
})

it('should scroll list on activeIndex changes', () => {
  const items = ["one", "two", "three", "four", "five"]
  const wrapper = mount(<AutoScrolledList items={items} className="test" itemClassName="test-item"/>)
  const listMock = {scrollHeight: 100, scrollTop: 0}

  wrapper.instance().refs.list = listMock

  wrapper.setProps({activeIndex: 3})
  expect(listMock.scrollTop).toEqual(40)

  wrapper.setProps({activeIndex: 1})
  expect(listMock.scrollTop).toEqual(0)
})
