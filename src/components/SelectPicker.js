import React, { Component } from 'react';
import { array, object, string, func } from 'prop-types'

import '../style/SelectPicker.css'

import AutoScrolledList from './AutoScrolledList'

class SelectPicker extends Component {
  static propTypes = {
    placeholder: string,
    items: array,
    alias: object,
    value: string,
    onChange: func
  }

  constructor(props) {
    super(props)

    this.state = {
      isListShown: false,
      activeIndex: 0
    }

    this.onPick = this.onPick.bind(this)
    this.onChange = this.onChange.bind(this)
    this.onFocus = this.onFocus.bind(this)
    this.onBlur = this.onBlur.bind(this)
    this.onKeyDown = this.onKeyDown.bind(this)
  }

  render() {
    const {isListShown, activeIndex} = this.state
    const {value, placeholder} = this.props
    const itemsToShow = this.getItemsToShow()

    const listClass = `select-picker__list select-picker__list_${isListShown ? 'shown': 'hidden'}`
    const itemClass = 'select-picker__list-item'

    return (
      <div className="select-picker">
        <input
          className="select-picker__input"
          value={value}
          placeholder={placeholder}
          onChange={this.onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          onKeyDown={this.onKeyDown}
        />
        <AutoScrolledList
          className={listClass}
          itemClassName={itemClass}
          activeIndex={activeIndex}
          items={itemsToShow}
          onSelect={this.onPick}
        />
      </div>
    );
  }

  getItemsToShow() {
    const {value, items, alias} = this.props
    const fitTheQuery = item => item.toLowerCase().startsWith(value.toLowerCase())
    const possibleAlias = Object.keys(alias).filter(fitTheQuery).map(key => alias[key])

    return items.filter(item => fitTheQuery(item) || possibleAlias.includes(item)).sort()
  }

  onKeyDown({keyCode}) {
    const handler = ({
      38: this.onMoveUp,
      40: this.onMoveDown,
      13: this.onEnter
    })[keyCode]

    if (handler instanceof Function) {
      handler.call(this)
    }
  }

  onMoveDown() {
    this.setState(({activeIndex}) => ({activeIndex: activeIndex === this.props.items.length - 1 ? 0 : activeIndex + 1}))
  }

  onMoveUp() {
    this.setState(({activeIndex}) => ({activeIndex: activeIndex === 0 ? this.props.items.length - 1 : activeIndex - 1}))
  }

  onEnter() {
    const value = this.getItemsToShow()[this.state.activeIndex]
    if (value) {
      this.onPick(value)
    }
  }

  onChange({target}) {
    this.setState({isListShown: true, activeIndex: 0})
    this.props.onChange(target.value)
  }

  onFocus() {
    this.setState({isListShown: true})
  }

  onBlur() {
    // Defer blur to allow the click event on item to be fired
    setTimeout(() => this.setState({isListShown: false, activeIndex: 0}), 150)
  }

  onPick(item) {
    this.setState({isListShown: false, activeIndex: 0})
    this.props.onChange(item)
  }
}

export default SelectPicker;
