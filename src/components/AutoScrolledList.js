import React, { Component } from 'react';
import { func, string, array, number } from 'prop-types'

import '../style/SelectPicker.css'

class AutoScrolledList extends Component {
  static propTypes = {
    className: string,
    itemClassName: string,
    items: array,
    onSelect: func,
    activeIndex: number
  }

  componentWillUpdate({activeIndex}) {
    if (this.props.activeIndex !== activeIndex) {
      this.refs.list.scrollTop = (this.refs.list.scrollHeight / this.props.items.length) * (activeIndex - 1)
    }
  }

  render() {
    const {items, className, itemClassName, onSelect, activeIndex} = this.props

    return (
      <ul className={className} ref="list">
        {items.map((item, i) => (
          <li
            ref={`item${i}`}
            onClick={() => onSelect(item)}
            className={`${itemClassName} ${activeIndex === i ? itemClassName + '_active' : ''}`}
            key={i}>
            {item}
          </li>
        ))}
      </ul>
    );
  }

}

export default AutoScrolledList;
